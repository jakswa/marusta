FROM messense/rust-musl-cross:x86_64-musl as builder
ADD . /home/rust/src
RUN apt-get update && apt-get install -y python jq && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN rustup update nightly && \
    rustup target add --toolchain nightly x86_64-unknown-linux-musl
RUN mv $(cargo +nightly build --target=x86_64-unknown-linux-musl --release --tests --message-format=json | jq -r "select(.profile.test == true) | .filenames[]") ./built_tests
RUN cargo +nightly build --target=x86_64-unknown-linux-musl --release

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
RUN mkdir resources
# COPY --from=builder /home/rust/src/resources ./resources
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/marusta .
COPY --from=builder /home/rust/src/built_tests .
ENTRYPOINT ["./marusta"]
